package com.example.spring.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.spring.datajpa.model.Employe;

public interface EmployeRepository extends JpaRepository<Employe, Long> {
	List<Employe> findByNameContaining(String title);
}
